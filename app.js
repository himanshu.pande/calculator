const cal = require("./cal");
const math = require("math");

const args = require('minimist')(process.argv.slice(2));

global.calMath = math;

const calFunct = args.funct;

const calArguments = args._;

if(calFunct == "add"){
	console.log(cal.add(calArguments[0],calArguments[1]));
}else if(calFunct == "multiple"){
	console.log(cal.multiple(calArguments[0],calArguments[1]));	
}else if(calFunct == "subtract"){
	console.log(cal.subtract(calArguments[0],calArguments[1]));
}else if(calFunct == "cube"){
	console.log(cal.cube(calArguments[0]));
}else{
	console.log("Invalid Function");
}