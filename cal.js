
const add = function (a,b){
	return a + b;
}

const subtract = function (a,b){
	return a - b;
}

const multiple = function (a,b){
	return a * b;
}

const cube = function (a){
	return calMath.pow(a,1/3);
}

module.exports = {
	add : add,
	subtract : subtract,
	multiple : multiple,
	cube : cube
};